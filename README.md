# notes

- Drones

	Navio 2 Shield

	 on a Raspberry Pi 3

	using emild-rasbian - jessy

	running APM-Navio2 

	Names: navio1, navio2

	Login: pi

	Password: raspberry

	IP addresses reserved DHCP

	navio1: 192.168.0.101

	navio2: 192.168.0.102

	hostnames:

	 [navio1.local](http://navio1.local) 

	 [navio2.local](http://navio2.local) 

- Wireless DIR-655 Router

	Web interface

	IP: 192.168.0.1

	Login: Admin

	Password: protokoll

	 **Wireless** 

	Wireless SSID: BevLab

	WPA2 Key: protokoll

	DHCP range : 192.168.0.100 - 200

- ROS -Qualisys

	[KumarRobotics/motion_capture_system](https://github.com/KumarRobotics/motion_capture_system)

	Body name in Qualisys should not contain problematic characters like space

- ROS-Server

	Ubuntu 16.04 x64

	ROS Kinetic Kame

	Login: blr

	Password: proto

	hostname: ROS-Server

	IP: 192.168.0.197

- misc

	roscore & 

	killall roscore

- sources/docs

	 [http://dev.px4.io/](http://dev.px4.io/) px4 is also uses mavros


